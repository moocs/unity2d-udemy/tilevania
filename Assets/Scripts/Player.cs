﻿using System;
using UnityEngine;

// using UnityStandardAssets.CrossPlatformInput;

public class Player : MonoBehaviour
{
    [SerializeField] private float horizSpeed = 5f;
    [SerializeField] private float jumpSpeed = 5f;
    [SerializeField] private float climbSpeed = 3f;
    [SerializeField] private Vector2 deathKick = new Vector2(3, 15);


    private Animator _animator;
    private Rigidbody2D _rigidbody2D;
    private CapsuleCollider2D _collider2Dbody;
    private BoxCollider2D _collider2Dfeet;
    private GameSession _gameSession;

    private float _stdGravity;
    private bool _isJumping;
    private bool _isAlive = true;
    private static readonly int IsClimbing = Animator.StringToHash("IsClimbing");
    private static readonly int Jumping = Animator.StringToHash("Jumping");
    private static readonly int IsWalking = Animator.StringToHash("IsWalking");
    private static readonly int Dying = Animator.StringToHash("Dying");


    // Start is called before the first frame update
    void Start()
    {
        _animator = GetComponent<Animator>();
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _collider2Dbody = GetComponent<CapsuleCollider2D>();
        _collider2Dfeet = GetComponent<BoxCollider2D>();
        _gameSession = FindObjectOfType<GameSession>();


        _stdGravity = _rigidbody2D.gravityScale;
    }

    // Update is called once per frame
    void Update()
    {
        if (!_isAlive) return;

        Run();
        Jump();
        ClimbLadder();
        Die();
    }

    void Die()
    {
        var isTouchingEnemy = _collider2Dbody.IsTouchingLayers(LayerMask.GetMask("Enemy", "Hazard"));
        if (!isTouchingEnemy) return;

        _isAlive = false;
        _animator.SetTrigger(Dying);
        _rigidbody2D.velocity = deathKick;

        _gameSession.ProcessPlayerDeath();
    }

    private void ClimbLadder()
    {
        var isTouchingLadder = _collider2Dbody.IsTouchingLayers(LayerMask.GetMask("Ladder"));

        if (!isTouchingLadder)
        {
            _animator.SetBool(IsClimbing, false);
            _rigidbody2D.gravityScale = _stdGravity;
            return;
        }

        // Setting gravity to 0 so player do not goes down on ladder
        _rigidbody2D.gravityScale = 0;

        var deltaY = Input.GetAxis("Vertical") * climbSpeed;
        _rigidbody2D.velocity = new Vector2(_rigidbody2D.velocity.x, deltaY);

        var isWalking = Math.Abs(deltaY) > Mathf.Epsilon;
        _animator.SetBool(IsClimbing, isWalking);
    }

    private void Jump()
    {
        var isTouchingGround = _collider2Dfeet.IsTouchingLayers(LayerMask.GetMask("Foreground"));
        var jumpButtonPressed = Math.Abs(Input.GetAxis("Jump")) > Mathf.Epsilon;

        if (!isTouchingGround) return;
        if (!jumpButtonPressed) return;

        _rigidbody2D.velocity = new Vector2(_rigidbody2D.velocity.x, jumpSpeed);
        _animator.SetTrigger(Jumping);
    }

    private void Run()
    {
        var transform1 = transform;
        var localScale = transform1.localScale;

        // Translate way
        // var deltaX = Input.GetAxis("Horizontal") * Time.deltaTime * horizSpeed;
        // transform1.Translate(Vector3.right * deltaX);

        // Rigibody velocity way
        var deltaX = Input.GetAxis("Horizontal") * horizSpeed;
        _rigidbody2D.velocity = new Vector2(deltaX, _rigidbody2D.velocity.y);


        var isWalking = Math.Abs(deltaX) > Mathf.Epsilon;
        _animator.SetBool(IsWalking, isWalking);
        if (isWalking)
        {
            transform1.localScale = new Vector2(Mathf.Sign(deltaX), localScale.y);
        }
    }
}