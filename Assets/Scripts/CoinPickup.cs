﻿using UnityEngine;

public class CoinPickup : MonoBehaviour
{
    [SerializeField] private AudioClip pickupAudio;
    [SerializeField] private int scoreValue = 100;

    private GameSession _gameSession;

    private void Start()
    {
        _gameSession = FindObjectOfType<GameSession>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        _gameSession.AddToScore(scoreValue);
        
        if (Camera.main != null)
            AudioSource.PlayClipAtPoint(pickupAudio, Camera.main.transform.position + Vector3.forward);
        Destroy(gameObject);
    }
}