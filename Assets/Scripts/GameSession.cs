﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameSession : MonoBehaviour
{
    [SerializeField] private int playerLives = 3;
    [SerializeField] private Text scoreText;
    [SerializeField] private Text livesText;

    private int _currentLives;
    private int _currentScore;

    private void Awake()
    {
        Singleton();
    }

    private void Singleton()
    {
        var numGameSession = FindObjectsOfType<GameSession>().Length;

        if (numGameSession > 1)
        {
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
        }
    }


    // Start is called before the first frame update
    void Start()
    {
        CurrentLives = playerLives;
        CurrentScore = 0;
    }

    private int CurrentLives
    {
        get => _currentLives;
        set
        {
            _currentLives = value;
            livesText.text = CurrentLives.ToString();
        }
    }

    private int CurrentScore
    {
        get => _currentScore;
        set
        {
            _currentScore = value;
            scoreText.text = CurrentScore.ToString();
        }
    }

    public void AddToScore(int scoreToAdd)
    {
        CurrentScore += scoreToAdd;
    }


    public void ProcessPlayerDeath()
    {
        StartCoroutine(CurrentLives >= 1 ? TakeLife() : ResetGameSession());
    }

    private IEnumerator TakeLife()
    {
        CurrentLives--;
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private IEnumerator ResetGameSession()
    {
        yield return new WaitForSeconds(5f);
        SceneManager.LoadScene(0);
        Destroy(gameObject);
    }
}