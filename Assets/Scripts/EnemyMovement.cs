﻿using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    [SerializeField] private float moveSpeed = 1f;

    private Rigidbody2D _rigidbody2D;
    private bool _walkToTheRight = true;

    // Start is called before the first frame update
    void Start()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        Walk();
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        _walkToTheRight = transform.localScale.x < 0;
    }

    private void Walk()
    {
        if (_walkToTheRight)
        {
            transform.localScale = new Vector3(1, 1, 1);
            _rigidbody2D.velocity = Vector2.right * moveSpeed;
        }
        else
        {
            transform.localScale = new Vector3(-1, 1, 1);
            _rigidbody2D.velocity = Vector2.left * moveSpeed;
        }
    }
}