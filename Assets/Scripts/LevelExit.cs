﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelExit : MonoBehaviour
{
    [SerializeField] private float loadLevelDelay = 5f;
    [SerializeField] private float levelExitSlowMoFactor = .2f;


    private void OnTriggerEnter2D(Collider2D other)
    {
        StartCoroutine(LoadNextSceneWithDelay());
    }

    IEnumerator LoadNextSceneWithDelay()
    {
        var oldTimeScale = Time.timeScale;
        Time.timeScale = levelExitSlowMoFactor * oldTimeScale;
        yield return new WaitForSecondsRealtime(loadLevelDelay);

        Time.timeScale = oldTimeScale;
        var currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentSceneIndex + 1);
    }
}